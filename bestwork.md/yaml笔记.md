# Yaml学习笔记
### Yaml的意义：
- YAML以数据为中心，使用空白，缩进，分行组织数据，从而使得表示更加简洁易读。YAML（发音 /ˈjæməl/）是一个类似 XML、JSON 的数据序列化语言，是专门用来写配置文件的语言，非常简洁和强大，号称“一种人性化的数据格式语言”。
### Yaml格式示例：
##### 多行缩进
- 数据结构可以用类似大纲的缩排方式呈现，结构通过 缩进 来表示，连续的项目通过 减号“-”来表示，map 结构里面的 key/value 对用冒号“:”来分隔。样例如下：

![](https://gitee.com/qzbella/yaml_learning_notes/raw/master/images/1.png)

##### 单行缩写
- YAML也有用来描述好几行相同结构的数据的缩写语法，数组用'[]'包括起来，hash用'{}'来包括。因此，上面的这个YAML能够缩写成这样:

![](https://gitee.com/qzbella/yaml_learning_notes/raw/master/images/2.png)

### Yaml语法的基础规则
- 大小写敏感
- 使用缩进表示层级关系
- 禁止使用tab缩进，只能使用空格键
- 缩进长度没有限制，只要元素对齐就表示这些元素属于一个层级。
- 使用#表示注释
- 字符串可以不用引号标注
