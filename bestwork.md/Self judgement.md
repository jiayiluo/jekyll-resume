通过网络技术与协作这门课程，我了解到HTTP、TCP、IP协议的相关知识，通过实操了解gitee和GitHub的相关实践。通过这次期末实操对pr、merge等操作更熟练。  通过做读书笔记来对相关知识点进行巩固。  
**week15后commit：40次**  
- [gitee实操文档](https://gitee.com/jiayiluo/jekyll-resume/tree/master/bestwork.md)
- [json](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/Top%2010%20college.json) 
- [csv](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/Top%2010%20college.csv)
- [tsv](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/Top%2010%20college.tsv)
- [yaml](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/Top%2010%20college.yaml) 
- [xml](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/Top%2010%20college.xml)  
- [json csv tsv yaml xml意义用途](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/csv%20json%20yaml%20xml%E6%84%8F%E4%B9%89%E7%94%A8%E9%80%94.md)  
**笔记**
- [HTTP](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/HTTP%E8%AF%BB%E4%B9%A6%E7%AC%94%E8%AE%B0.md)
- [TCP IP](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/TCP%20IP%20%E7%AC%94%E8%AE%B0.md)  
- [网络素养](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/%E7%BD%91%E7%BB%9C%E7%B4%A0%E5%85%BB%E7%AC%94%E8%AE%B0.md)  