* 罗嘉怡
* Gitee账号：jiayiluo

一、[Commits数量及频率](https://gitee.com/jiayiluo) ：

最近一年贡献: 48 次，最长连续贡献: 3 日，最近连续贡献: 3 日

二、文档格式丶意义丶及用途

* md  
  * [实操](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/Self%20judgement.md)  
  * [笔记](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/md%E7%AC%94%E8%AE%B0.md)

* json  
  * [实操](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/Top%2010%20college.json)  
  * [笔记](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/json%E7%AC%94%E8%AE%B0.md)

* yaml 
  * [实操](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/Top%2010%20college.yaml)  
  * [笔记](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/yaml%E7%AC%94%E8%AE%B0.md)

* tsv 
  * [实操](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/Top%2010%20college.tsv)  
  * [笔记](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/tsv%E7%AC%94%E8%AE%B0.md)

* csv  
  * [实操](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/Top%2010%20college.csv)  
  * [笔记](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/csv%E7%AC%94%E8%AE%B0.md)

三、[参与科技与共享文化](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/%E7%BD%91%E7%BB%9C%E7%B4%A0%E5%85%BB%E7%AC%94%E8%AE%B0.md)

四、 [HTTP笔记](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/HTTP%E8%AF%BB%E4%B9%A6%E7%AC%94%E8%AE%B0.md) [TCP/IP笔记](https://gitee.com/jiayiluo/jekyll-resume/blob/master/bestwork.md/TCP%20IP%20%E7%AC%94%E8%AE%B0.md)
