# TCP/IP（通信协议）
![网络基础知识](https://upload-images.jianshu.io/upload_images/15517767-c01f256086505614.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/644/format/webp)  
■ 应用层
为应用程序提供服务并规定应用程序中通信相关的细节。包括文件 传输、电子邮件、远程登录（虚拟终端）等协议。  
■ 表示层
将应用处理的信息转换为适合网络传输的格式，或将来自下一层的 数据转换为上层能够处理的格式。因此它主要负责数据格式的转换。
具体来说，就是将设备固有的数据格式转换为网络标准传输格式。 不同设备对同一比特流解释的结果可能会不同。因此，使它们保持一致 是这一层的主要作用。  
■ 会话层
负责建立和断开通信连接（数据流动的逻辑通路），以及数据的分 割等数据传输相关的管理。  
■ 传输层
起着可靠传输的作用。只在通信双方节点上进行处理，而无需在路 由器上处理。  
■ 网络层
将数据传输到目标地址。目标地址可以是多个网络通过路由器连接 而成的某一个地址。因此这一层主要负责寻址和路由选择。  
■ 数据链路层
负责物理层面上互连的、节点之间的通信传输。例如与1个以太网 相连的2个节点之间的通信。
将0、1序列划分为具有意义的数据帧传送给对端（数据帧的生成与 接收）。  
■ 物理层
负责0、1比特流（0、1序列）与电压的高低、光的闪灭之间的互换  
## 计算机与网络发展的7个阶段  
![计算机使用模式的演变](https://upload-images.jianshu.io/upload_images/15517767-ee5ccdd63b071a03.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1000/format/webp)
## 相关协议的介绍
![相关协议](https://upload-images.jianshu.io/upload_images/15517767-3fe8df13ffbd8381.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1000/format/webp)  
 -  H.323  此协议是由ITU开发用于在IP网上传输音频、视频的一种协议。
起初，它主要是作为接入ISDN网和IP网之上的电话网为目的的一种规范而被提出的。, 

 - TCP 此协议是辅助Real-Time Protocol的一种协议。通过丢包率等线路质量的管理，对Real-Time Protocol的数据传送率进行控制。   

- LDAP 它是访问目录服务的一种协议，也叫轻量级目录访问协议。所谓“目录服务”是指网络上存在的一种提供相关资源的数据库的服务。   

- FTP 此协议是在两个相连的计算机之间进行文件传输时使用的协议。用于控制的TCP连接主要在此协议的控制部分使用。例如登录用户名和密码的验证、发送文件的名称、发送方式的设置。利用这个连接，可以通过ASCII码字符串发送请求和接收应答。  

 - SIP 它相当于OSI参考模型中的会话层。终端之间进行多媒体通信时，需要具备事先解析对方地址、呼出对方号码并对所要传输的媒体信息进行处理等功能。   

- P2P 网络上的终端或主机不经服务器直接1对1相互通信的情况。使用它以后，可以分散音频数据给网络带来的负荷，实现更高效的应用。   

- POP 可以根据接收端的请求发送邮件的协议。该协议是一种用于接收电子邮件的协议。  

## TCP/IP分层模型与通信示例
![数据包首部的层次化](https://image.slidesharecdn.com/tcp-140613123317-phpapp01/95/tcp-ip-28-638.jpg?cb=1402662871)
•框 包、帧、数据报、段、消息  
◦packets  
◦frames  
◦data packets  
◦segment  
◦messages  
## 关于JavaScript、CGI、Cookie、DOM、Ajax的介绍  
![介绍](https://upload-images.jianshu.io/upload_images/15517767-2b7f4d2aefe85611.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1000/format/webp)  
• Cookie Web应用中为了获取用户信息使用一个机制。它常被用于保存登录信息或网络购物中放入购物车的商品信息。
   
•Really Simple Syndication RSS 用来交互与Web站点内容更新相关的摘要信息的一种数据格式。如果使用此数据格式，则可以将页面的标题、内容中的章节标题和概要、分类、关键字等信息记述下来，只显示页面的概要，提高关键字搜索的精度。  

• Common Gateway Interface CGI 是Web服务器调用外部程序时所使用的一种服务端应用的规范。引入此规范以后客户端请求会触发Web服务器端运行另一个程序，客户端所输入的数据也会传给这个外部程序。

• Asynchronous JavaScrip and XML Ajax 是指一种创建交互式网页应用的网页开发技术。在无需重新加载整个网页的情况下，能够更新部分网页的。

 •Document Object Model DOM 文档对象模型，是W3C组织推荐的处理可扩展标志语言的标准编程接口。在网页上，组织页面（或文档）的对象被组织在一个树形结构中，用来表示文档中对象的标准模型。

 •JavaScript 是一种嵌入在HTML中的编程语言，作为客户端程序可以运行于多种类型的浏览器中。这种程序用于验证客户端输入字符串是否过长、是否填写或选择了页面中的必须选项等功能。
## TCP/IP的标准化流程
![流程图](https://upload-images.jianshu.io/upload_images/15517767-e137f33d2ad176b5.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/533/format/webp)  
## 网络安全构成要素
![要素构成图](https://upload-images.jianshu.io/upload_images/15517767-559edd451a16990e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/688/format/webp)  